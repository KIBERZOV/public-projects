﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSearchManager
{
    public partial class Form1 : Form
    {
        EventWaitHandle ew;
        bool Stop, Pause;

        //Базовый путь до проводника
        string BasePath = @"";
        //Имя файла для поиска
        string FileName = "";
        //Расширение файла для поиска
        string FileExtension = "";
        //Текст в файле для поиска
        string FileText = "";
        //Путь для поиска файлов
        string SearchPath = @"";
        //Полное имя файла
        string FullFileName = "";
        //Путь файла для постройки дерева папок
        string FilePath = @"";
        //Отключение обработчика во время поиска
        bool Selected = true;
        //Секунды
        int S = 0;
        //Минуты
        int M = 0;
        //Часы
        int H = 0;
        //Массив для двойного поиска
        string[] FullFilePath_Cout;

        public Form1()
        {
            InitializeComponent();
            //Установка базового пути проводника
            SetBasePath();
            //Установка базового пути для TreeView
            DriveTreeInit();
            //Управление состоянием потока
            ew = new EventWaitHandle(false, EventResetMode.AutoReset);
            Stop = true;
        }

        //Установка базового пути проводника
        private void SetBasePath()
        {
            //Записываем доступные диски в combobox
            foreach (string disk in Environment.GetLogicalDrives())
            {
                cmbDrives.Items.Add(disk);
            }
            //Выбираем первый диск
            cmbDrives.SelectedIndex = 0;
            //Записываем базовый путь до проводника
            BasePath = cmbDrives.SelectedItem.ToString();
            //Выводим в браузер базовый путь
            webBrowser.Url = new Uri(BasePath);
            //Выводим в строку поиска базовый путь
            txtPath.Text = BasePath;
            //Выводим в строку поиска файлов базовый путь
            cmbFilePath.Items.Add(BasePath);
            cmbFilePath.SelectedIndex = 0;
        }

        //Обновляем проводник в зависимости от выбранного диска
        private void CmbDrives_SelectedValueChanged(object sender, EventArgs e)
        {
            if(cmbDrives.Text == cmbDrives.Text)
            {
                //Обновляем базовый путь
                BasePath = cmbDrives.Text;
                //Обновляем проводник
                webBrowser.Url = new Uri(BasePath);
                //Обновляем строку поиска
                txtPath.Text = BasePath;
                //Обновляем строку поиска файлов
                cmbFilePath.Text = BasePath;
            }
        }

        //Путь для поиска файлов
        private void BtnOpenPath_Click(object sender, EventArgs e)
        {
            using(FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Выберите интересующий вас каталог." })
            {
                if(fbd.ShowDialog() == DialogResult.OK)
                {
                    cmbFilePath.Items.Add(fbd.SelectedPath);
                    cmbFilePath.Text = fbd.SelectedPath;
                    SearchPath = fbd.SelectedPath;
                }
            }
        }

        //Управление поиском текста в файле
        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                cmbFileText.Enabled = true;
                cmbFileExist.Text = "txt";
                cmbFileExist.Enabled = false;
            }
            else if(checkBox1.Checked == false)
            {
                cmbFileText.Enabled = false;
                cmbFileExist.Text = "";
                cmbFileText.Text = "";
                cmbFileExist.Enabled = true;
            }
        }

        //Строим дерево папок до найденного файла
        private void PopulateTreeViewWithPath(TreeView TV, string FilePath)
        {
            //Отключаем отрисовку
            treeView.BeginUpdate();
            //Создаем пустую ноду
            TreeNode LastNode = null;
            //Разделяем путь на массив папок
            string[] Parts = FilePath.Split('\\');
            //Создаем пустую строку
            string CummPath = String.Empty;
            //Перебираем папки
            foreach (string Part in Parts)
            {
                //Добавляем их к переменной, получая текущий путь
                CummPath += Part;
                //Проверяем их на ноль
                if (LastNode == null)
                {
                    //Добавляем в ноду еще ноду и путь к ней
                    LastNode = TV.Nodes.Add(CummPath, Part);
                }
                else
                {
                    //Добавляем в ноду еще ноду и путь к ней
                    LastNode = LastNode.Nodes.Add(CummPath, Part);
                    CummPath += '\\';
                }
                //Разрешаем перерисовку иерархического представления.
                treeView.EndUpdate();
            }
        }

        //Инициализация списка дисковых устройств
        public void DriveTreeInit()
        {
            string[] drivesArray = Directory.GetLogicalDrives();

            treeView.BeginUpdate();
            treeView.Nodes.Clear();

            foreach (string s in drivesArray)
            {
                TreeNode drive = new TreeNode(s, 0, 0);
                treeView.Nodes.Add(drive);

                GetDirs(drive);
            }
            treeView.EndUpdate();
        }

        //Получение списка каталогов
        public void GetDirs(TreeNode node)
        {
            DirectoryInfo[] diArray;

            node.Nodes.Clear();

            string fullPath = node.FullPath;
            DirectoryInfo di = new DirectoryInfo(fullPath);

            try
            {
                diArray = di.GetDirectories();
            }
            catch
            {
                return;
            }

            foreach (DirectoryInfo dirinfo in diArray)
            {
                TreeNode dir = new TreeNode(dirinfo.Name, 1, 2);
                node.Nodes.Add(dir);
            }
        }

        //Подгрузка каталогов
        private void TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if(Selected)
            {
                treeView.BeginUpdate();
                foreach (TreeNode node in e.Node.Nodes)
                {
                    GetDirs(node);
                }
                treeView.EndUpdate();
            }
        }

        //Отображение выбранного каталога
        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if(Selected)
            {
                TreeNode selectedNode = e.Node;
                string FullPath = selectedNode.FullPath;
                FullPath = FullPath.Remove(2, 1);
                try
                {
                    webBrowser.Url = new Uri(FullPath);
                    txtPath.Text = FullPath;
                }
                catch
                {
                    return;
                }
            }
        }

        //Возврат браузера на шаг назад
        private void BtnBack_Click(object sender, EventArgs e)
        {
            if(webBrowser.CanGoBack)
            {
                webBrowser.GoBack();
            }
        }

        //Возврат браузера на шаг вперед
        private void BtnForward_Click(object sender, EventArgs e)
        {
            if(webBrowser.CanGoForward)
            {
                webBrowser.GoForward();
            }
        }
        
        //Добавление файлов в кеш памяти
        private void AddScan(string FileName, string FileExtension, string SearchPath, string FileText)
        {
            //Проверяем блок имен файлов
            if(!(cmbFileName.Items.Cast<string>().Contains(FileName)))
            {
                cmbFileName.Items.Add(FileName);
            }
            //Проверяем блок расширений файлов
            if (!(cmbFileExist.Items.Cast<string>().Contains(FileExtension)))
            {
                cmbFileExist.Items.Add(FileExtension);
            }
            //Проверяем блок путей поиска файлов
            if (!(cmbFilePath.Items.Cast<string>().Contains(SearchPath)))
            {
                cmbFilePath.Items.Add(SearchPath);
            }
            //Проверяем блок текста файлов
            if (!(cmbFileText.Items.Cast<string>().Contains(FileText)))
            {
                cmbFileText.Items.Add(FileText);
            }
        }

        //Старт поиска файлов
        private void BtnPlay_Click(object sender, EventArgs e)
        {
            //Устонавливаем начальные значения для поиска
            FileName = cmbFileName.Text;
            FileExtension = cmbFileExist.Text;
            SearchPath = cmbFilePath.Text;
            FileText = cmbFileText.Text;
            AddScan(FileName, FileExtension, SearchPath, FileText);

            if (Stop)
            {
                if((FileName == "")&&(FileExtension == "")&&(SearchPath =="")&&(FileText == ""))
                {
                    txtFileName.Text = "Введите данные для поиска!";
                    return;
                }
                if((FileName != "")&&(FileExtension != "")&&(SearchPath == "")&&(FileText != ""))
                {
                    txtFileName.Text = "Введите путь для поиска файлов!";
                    return;
                }
                if((FileName != "")&&(FileExtension == "")&&(SearchPath != "")&&(FileText == ""))
                {
                    Selected = false;
                    FullFileName = FileName + "*";
                    Stop = false;
                    btnPause.Enabled = btnStop.Enabled = true;
                    btnPlay.Enabled = false;
                    treeView.Nodes.Clear();
                    ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                    {
                        NameAndExtension(FullFileName, FileExtension, SearchPath);
                        Time(S, M, H);
                    }));


                }
                if((FileName == "")&&(FileExtension != "")&&(SearchPath != "")&&(FileText == ""))
                {
                    Selected = false;
                    FileName = "*.";
                    FullFileName = FileName + FileExtension;
                    Stop = false;
                    btnPause.Enabled = btnStop.Enabled = true;
                    btnPlay.Enabled = false;
                    treeView.Nodes.Clear();
                    ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                    {
                        NameAndExtension(FullFileName, FileExtension, SearchPath);
                        Time(S, M, H);
                    }));
                }
                if((FileName != "")&&(FileExtension != "")&&(SearchPath != "")&&(FileText == ""))
                {
                    Selected = false;
                    FullFileName = FileName + "." + FileExtension;
                    Stop = false;
                    btnPause.Enabled = btnStop.Enabled = true;
                    btnPlay.Enabled = false;
                    treeView.Nodes.Clear();
                    ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                    {
                        NameAndExtension(FullFileName, FileExtension, SearchPath);
                        Time(S, M, H);
                    }));
                }
                if((FileName == "")&&(FileExtension != "")&&(SearchPath != "")&&(FileText != ""))
                {
                    Selected = false;
                    FullFileName = "*.txt";
                    Stop = false;
                    btnPause.Enabled = btnStop.Enabled = true;
                    btnPlay.Enabled = false;
                    treeView.Nodes.Clear();
                    ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                    {
                        SearchFileByText(SearchPath, FileText, FullFileName);
                        Time(S, M, H);
                    }));
                }
                if((FileName != "")&&(FileExtension != "")&&(SearchPath != "")&&(FileText != ""))
                {
                    Selected = false;
                    FullFileName = FileName + ".txt";
                    Stop = false;
                    btnPause.Enabled = btnStop.Enabled = true;
                    btnPlay.Enabled = false;
                    treeView.Nodes.Clear();
                    ThreadPool.QueueUserWorkItem(new WaitCallback((s) =>
                    {
                        SearchFileByText(SearchPath, FileText, FullFileName);
                        Time(S, M, H);
                    }));
                }
            }
            else
            {
                ew.Set();
                btnStop.Enabled = true;
            }
        }

        //Пауза поиска файлов
        private void BtnPause_Click(object sender, EventArgs e)
        {
            Pause = true;
            btnStop.Enabled = false;
            btnPlay.Enabled = true;
            txtFileName.Text = "Поиск приостановлен!";
        }

        //Остановка поска файлов
        private void BtnStop_Click(object sender, EventArgs e)
        {
            Stop = true;
            btnPause.Enabled = false;
            btnPlay.Enabled = true;
            txtFileName.Text = "Поиск остановлен!";
            S = 0;
            M = 0;
            H = 0;
        }

        //Очистка результатов поиска
        private void Button1_Click(object sender, EventArgs e)
        {
            Selected = true;
            lbLog.Items.Clear();
            treeView.Nodes.Clear();
            S = 0;
            M = 0;
            H = 0;
            lbS.Text = "00";
            lbM.Text = "00";
            lbH.Text = "00";
        }

        //Отображение в проводнике дерева дисков при выборе диска
        private void CmbDrives_SelectedIndexChanged(object sender, EventArgs e)
        {
            DriveTreeInit();
        }

        //Поиск файлов по тексту файла
        void SearchFileByText(string SearchPath, string FileText, string FulFileName)
        {
            //Событие для вывода результата в lbLog
            Action<string> Log = new Action<string>((v) =>
            {
                lbLog.Items.Add(v.ToString());
            });

            //Событие для вывода результата в txtFileName
            Action<string> StatusFile = new Action<string>((v) =>
            {
                txtFileName.Text = v.ToString();
            });

            //Получаем массиф файлов
            string[] GetFiles;
            try
            {
                GetFiles = Directory.GetFiles(SearchPath, FullFileName, SearchOption.AllDirectories);
            }
            catch
            {
                return;
            }

            //Проверяем файлы на их текст
            foreach(string j in GetFiles)
            {
                if (Pause)
                {
                    ew.WaitOne();
                    Pause = false;
                }
                //Читаем файл
                string tmp = File.ReadAllText(j);
                if(tmp.IndexOf(FileText, StringComparison.InvariantCulture)!= -1)
                {
                    if (lbLog.InvokeRequired)
                        lbLog.Invoke(new MethodInvoker(() =>
                        {
                            Thread.Sleep(500);
                            Log(j);
                            StatusFile(j);
                            PopulateTreeViewWithPath(treeView, j);
                        }));
                    else
                    {
                        Thread.Sleep(500);
                        Log(j);
                        StatusFile(j);
                        PopulateTreeViewWithPath(treeView, j);
                    }
                }
            }
            if (btnStop.InvokeRequired)
                btnStop.Invoke(new MethodInvoker(() =>
                {
                    btnStop.PerformClick();
                }));
        }

        //Таймер
        void Time(int S, int M, int H)
        {
            //Обращение к секундам
            Action<int> S_ = new Action<int>((v) =>
            {
                lbS.Text = v.ToString();
            });

            //Обращение к минутам
            Action<int> M_ = new Action<int>((v) =>
            {
                lbM.Text = v.ToString();
            });

            //Обращение к часам
            Action<int> H_ = new Action<int>((v) =>
            {
                lbH.Text = v.ToString();
            });

            while(!Stop)
            {
                if(Pause)
                {
                    ew.WaitOne();
                    Pause = false;
                }
                Thread.Sleep(1000);
                S += 1;
                if(S == 60)
                {
                    M += 1;
                    S = 0;
                }
                if(M == 60)
                {
                    H += 1;
                    M = 0;
                    S = 0;
                }

                if (lbS.InvokeRequired)
                    lbS.Invoke(new MethodInvoker(() =>
                    {
                        S_(S);
                        M_(M);
                        H_(H);
                    }));
                else
                {
                    S_(S);
                    M_(M);
                    H_(H);
                }
            }
        }

        //Поиск по имени и расширению файла
        void NameAndExtension(string FullFileName, string FileExtension, string SearchPath)
        {
            //Событие для вывода результата в lbLog
            Action<string> Log = new Action<string>((v) =>
            {
                lbLog.Items.Add(v.ToString());
            });

            //Событие для вывода результата в txtFileName
            Action<string> StatusFile = new Action<string>((v) =>
            {
                txtFileName.Text = v.ToString();
            });

            //Получаем массив файлов
            try
            {
                FullFilePath_Cout = Directory.GetFiles(SearchPath, FullFileName, SearchOption.AllDirectories);
            }
            catch
            {
                return;
            }
            //Получаем рузльтат для вывода
            foreach (string i in FullFilePath_Cout)
            {
                
                if (Pause)
                {
                    ew.WaitOne();
                    Pause = false;
                }
                if (lbLog.InvokeRequired)
                    lbLog.Invoke(new MethodInvoker(() =>
                    {
                        Thread.Sleep(500);
                        StatusFile(i);
                        Log(i);
                        PopulateTreeViewWithPath(treeView, i);
                    }));
                else
                {
                    Thread.Sleep(500);
                    StatusFile(i);
                    Log(i);
                    PopulateTreeViewWithPath(treeView, i);
                }
            }
            if (btnStop.InvokeRequired)
                btnStop.Invoke(new MethodInvoker(() =>
                {
                    btnStop.PerformClick();
                }));
        }

        //Отображение найденных объектов
        private void LbLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            webBrowser.Url = new Uri(lbLog.SelectedItem.ToString());
            txtPath.Text = lbLog.SelectedItem.ToString();
        }
    }
}
